from django.test import TestCase

from dogs.forms import DogForm, Dog

# Create your tests here.

DOGS_TEST_DATA = [
    {'first_name': 'Rex', 'last_name': 'German'},
    {'first_name': 'Kuzya', 'last_name': ''},
    {'first_name': 'Kuzya', 'last_name': 'Rex'}
]


class DogModelTest(TestCase):
    """
    TestCase for the Dog model
    """
    def setUp(self):
        self.test_data = DOGS_TEST_DATA

        self.fail_data = self.test_data

    def test_success_create(self):
        """
        Test for success create of 3 Dog objects
        """
        for data in self.test_data:
            form = DogForm(data=data)
            self.assertTrue(form.is_valid())
            form.save()

        self.assertEqual(self.test_data.__len__(), Dog.objects.count())

    def test_fail_create(self):
        """
        Test for testings fails creating the Dog objects
        """
        for data in self.test_data:
            form = DogForm(data=data)
            self.assertTrue(form.is_valid())
            form.save()

        fails = 0
        for data in self.fail_data:
            form = DogForm(data=data)

            if not form.is_valid():
                fails += 1
            self.assertFalse(form.is_valid())

        self.assertEqual(Dog.objects.count(), fails)
