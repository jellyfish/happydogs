from django import forms
from django.core.exceptions import ValidationError

from dogs.models import Dog
from dogs.utils import full_name_check


class DogForm(forms.ModelForm):
    class Meta:
        model = Dog
        fields = ['first_name', 'last_name']

    def clean(self):
        valid_name = full_name_check(self.cleaned_data)
        if not valid_name:
            raise ValidationError('Full name of the Dog must be unique.')
