from dogs.models import Dog


def full_name_check(dog_name):
    """
    The help function for check the unique full_name of a Dog model
    :param dog_name: the dict with first/last name of the Dog object
    :return: True if free, False if already exists
    """
    return not Dog.objects.filter(**dog_name).exists()
