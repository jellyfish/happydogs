from django.contrib import admin

from dogs.models import Dog
from dogs.forms import DogForm


@admin.register(Dog)
class DogAdmin(admin.ModelAdmin):
    list_display = ['id', 'first_name', 'last_name']
    search_fields = ['id', 'first_name', 'last_name']
    form = DogForm
