$(document).ready(function(){
    $('.day').click(function(){
        var count = $(this).find('.dogs-count').text().split(' ')[0];

        if (count > 0) {
            $(this).find('.dogs-block').toggleClass('open');
        }
    });
});
